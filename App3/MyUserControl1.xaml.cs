﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace App3
{
    public sealed partial class MyUserControl1 : UserControl
    {
        public Button _btnOk;
        public TextBlock _txtLetras;
        public TextBox _txtCorreo;
        public TextBox _txtImporte;
        public CheckBox _chkMail;
        public Button _btnCancel;
        public MyUserControl1()
        {
            this.InitializeComponent();
            _btnOk = btnOK;
            _btnCancel = btnCancel;
            _txtCorreo = txtMail;
            _txtLetras = txtEnLetras;
            _chkMail = chkMail;
            _txtImporte = txtImporte;
        }

        private void btnOK_Loaded(object sender, RoutedEventArgs e)
        {
            _btnOk = (Button)sender;
        }

        private void btnCancel_Loaded(object sender, RoutedEventArgs e)
        {
            _btnCancel = (Button)sender;
        }

        private void chkMail_Checked(object sender, RoutedEventArgs e)
        {
            txtMail.IsEnabled = true;
            txtMail.Focus(FocusState.Keyboard);
        }

        private void chkMail_Unchecked(object sender, RoutedEventArgs e)
        {
            txtMail.Text = "";
            txtMail.IsEnabled = false;
        }
    }
}
