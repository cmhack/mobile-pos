﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App3.Common
{
    class VentaItem
    {
        public int id { get; set; }

        public string codigo { get; set; }
        public int cantidad { get; set; }
        public double precio { get; set; }
        public string nombre { get; set; }

        public string sPrecio
        {
            get
            {
                return this.cantidad + " x " + this.precio.ToString("$00.00");
            }
        }
    }

    class VentaHeader {

        private double _subtotal;
        private double _iva;
        private double _total;

        private double _granTotal;

        public int id { get; set; }

        public double subtotal { get {
            double mTot = 0;
            foreach (VentaItem mI in this.items)
            {
                mTot += (mI.precio * mI.cantidad);
            }

            return mTot;
        }
   
        }
        
        public double iva { 
            get{
                return this.subtotal * .16;
            }
        }

        public double total
        {
            get
            {
                return this.subtotal * 1.16;
            }
   
        }

 
        public List<VentaItem> items { get; set; }

        public  VentaHeader()
        {
            this.items = new List<VentaItem>();
        }

    }
}
