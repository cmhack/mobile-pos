﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App3.DataModel
{
    public class Prod
    {
        public int id { get; set; }
        public string cod { get; set; }
        public string name { get; set; }
        public double cost { get; set; }
  
        public override string ToString()
        {
            return this.name;
        }
    }

    public class RootProds
    {
        public List<Prod> prods { get; set; }
    }
}
