﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App3.DataModel
{
    public class Daysale
    {
        public int id { get; set; }
        public string fecha { get; set; }
        public float subtotal { get; set; }
        public float iva { get; set; }
        public float total { get; set; }
    }

    public class itemsales
    {
        public int id { get; set; }
        public string fecha { get; set; }
        public float subtotal { get; set; }
        public float iva { get; set; }
        public float total { get; set; }

        public String sTotal
        {
            get
            {
                return this.total.ToString("$ 00.00");
            }
        }

        public String sID
        {
            get { return this.id.ToString("D6"); }
        }
    }

    public class RootSales
    {
        public List<itemsales> daysales { get; set; }
        public List<itemsales> weeksales { get; set; }
    }
}
