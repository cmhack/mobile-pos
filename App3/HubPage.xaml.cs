﻿using App3.Common;
using App3.Data;
using App3.DataModel;
using Newtonsoft.Json;
using System;
using System.Windows;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using System.Net.Http;
using Windows.UI.Popups;
using System.Diagnostics;
using System.Collections.ObjectModel;


// The Hub Application template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace App3
{
    /// <summary>
    /// A page that displays a grouped collection of items.
    /// </summary>
    public sealed partial class HubPage : Page
    {
        private readonly NavigationHelper navigationHelper;
        private readonly ObservableDictionary defaultViewModel = new ObservableDictionary();
        private readonly ObservableDictionary todaySales = new ObservableDictionary();
        private readonly ResourceLoader resourceLoader = ResourceLoader.GetForCurrentView("Resources");

        private ObservableCollection<VentaItem> toVentas = new ObservableCollection<VentaItem>();
        private Prod _item;
        private TextBox _cant;
        private TextBlock _total;
        private TextBlock _iva;
        private TextBlock _subtotal;
        public  ListView _mList;
        private ComboBox _mCombo;
        private VentaHeader _myVenta = new VentaHeader();
        private MyUserControl1 control = new MyUserControl1();
        private Popup popup = new Popup();

        public HubPage()
        {
            this.InitializeComponent();

            // Hub is only supported in Portrait orientation
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;

            this.NavigationCacheMode = NavigationCacheMode.Required;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

           
          
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

       

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            // TODO: Create an appropriate data model for your problem domain to replace the sample data
            var client = new HttpClient(); // Add: using System.Net.Http;
            if(!this.DefaultViewModel.ContainsKey("Prods")){  
            var response = await client.GetAsync(new Uri("http://dev-apps.mx/wp/getProducts.php"));
            var result = await response.Content.ReadAsStringAsync();
            var root = JsonConvert.DeserializeObject<RootProds>(result);
                this.DefaultViewModel.Add("Prods", root);
            }

            if(!this.DefaultViewModel.ContainsKey("Dias")){
            var res2= await client.GetAsync(new Uri("http://dev-apps.mx/wp/getSalesByDay.php"));
            var rslt = await res2.Content.ReadAsStringAsync();
            
            var rootDays = JsonConvert.DeserializeObject<RootSales>(rslt);          
                this.DefaultViewModel.Add("Dias", rootDays);    
            }
            

            
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            // TODO: Save the unique state of the page here.
        }

        /// <summary>
        /// Shows the details of a clicked group in the <see cref="SectionPage"/>.
        /// </summary>
        private void GroupSection_ItemClick(object sender, ItemClickEventArgs e)
        {
            var groupId = ((VentaItem)e.ClickedItem).id;
            if (!Frame.Navigate(typeof(SectionPage), groupId))
            {
                throw new Exception(this.resourceLoader.GetString("NavigationFailedExceptionMessage"));
            }
        }

        /// <summary>
        /// Shows the details of an item clicked on in the <see cref="ItemPage"/>
        /// </summary>
        private void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {

            var groupId = ((itemsales)e.ClickedItem).id;
            if (!Frame.Navigate(typeof(SectionPage), groupId))
            {
                throw new Exception(this.resourceLoader.GetString("NavigationFailedExceptionMessage"));
            }
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion


        private void myProds_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _item = ((sender as ComboBox).SelectedItem as Prod);
            //System.Diagnostics.Debug.WriteLine("THIS IS IT " + _item.name);

            _mCombo = (ComboBox)sender;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int cantidad;
            ((Button)sender).IsEnabled = false;
            
            if (!string.IsNullOrWhiteSpace(_cant.Text))
            {

                try { 

                    cantidad = int.Parse(_cant.Text);

                    VentaItem item = new VentaItem();
                    item.id = _item.id;
                    item.codigo = _item.cod;
                    item.nombre = _item.name;
                    item.precio = _item.cost;
                    item.cantidad = cantidad;

                    _myVenta.items.Add(item);

                    _cant.Text = "";
                    _mCombo.SelectedIndex = -1;
                    

                    toVentas.Add(item);
                    _subtotal.Text = "Subtotal: " + _myVenta.subtotal.ToString("$#,#00.00");
                    _iva.Text = "IVA: " + _myVenta.iva.ToString("$#,#00.00");
                    _total.Text = "Total: " + _myVenta.total.ToString("$#,#00.00");
                    
                }catch (Exception ee)
                {
                    MessageDialog messageDialog = new MessageDialog("Valor no valido");
                    messageDialog.ShowAsync();
                }
            }
            ((Button)sender).IsEnabled = true;
        }

        private async void ShowPaymentPrompt() {


            popup.Height = 300;
            popup.Width = 450;
            popup.VerticalOffset = 100;
          
            control._txtLetras.Text = new Conversiones().enletras(_myVenta.total);
            popup.Child = control;
            popup.IsOpen = true;
           

            control._btnOk.Click += (s, args) =>
            {
                if (Double.Parse(control._txtImporte.Text) < _myVenta.total)
                    return;
                

                control._btnOk.Content = "Procesando";
                control._btnCancel.IsEnabled = false;
                control._btnOk.IsEnabled = false;
                EnviarDatos();
            };

            control._btnCancel.Click += (s, args) =>
            {
                popup.IsOpen = false;
            };

          
            

        }

        private void ShowConfirmation()
        {

        }

        private async  void  Enviar_Click(object sender, RoutedEventArgs e)
        {
            ShowPaymentPrompt();
        }

        private async void EnviarDatos()
        {
            var client = new HttpClient(); // Add: using System.Net.Http;
            var ser = JsonConvert.SerializeObject(_myVenta);

            var formContent = new FormUrlEncodedContent(new[]{
                        new KeyValuePair<string,string>("mail",control._txtCorreo.Text),
                        new KeyValuePair<string,string>("item",ser)
                    });

            var response = await client.PostAsync(new Uri("http://dev-apps.mx/wp/saveSale.php"), formContent);
            var resu = await response.Content.ReadAsStringAsync();
            Debug.WriteLine("DATA " + resu);
            if (resu == "OK")
            {
                try
                {
                    MessageDialog messageDialog = new MessageDialog("Venta Satisfactoria.\nCambio al cliente:" + (Double.Parse(control._txtImporte.Text) - _myVenta.total).ToString(" $00.00"));
                    messageDialog.ShowAsync();
                }
                catch (Exception e)
                { }
                popup.IsOpen = false;
                control = new MyUserControl1();
                _myVenta.items.Clear();
                toVentas.Clear();

                _subtotal.Text = "Subtotal: " + _myVenta.subtotal.ToString("$#,#00.00");
                _iva.Text = "IVA: " + _myVenta.iva.ToString("$#,#00.00");
                _total.Text = "Total: " + _myVenta.total.ToString("$#,#00.00");

                var nRes = await client.GetAsync(new Uri("http://dev-apps.mx/wp/getSalesByDay.php"));
                var nRt = await nRes.Content.ReadAsStringAsync();

                var rtDays = JsonConvert.DeserializeObject<RootSales>(nRt);
               // this.DefaultViewModel.Remove("Dias");
                this.DefaultViewModel["Dias"] = rtDays;

               // this.DefaultViewModel.Add("Dias", rootDays);
            }
           
        }

        private void txtCant_Loaded(object sender, RoutedEventArgs e)
        {
            _cant = (TextBox)sender;
        }

        private void myList_Loaded(object sender, RoutedEventArgs e)
        {
            _mList = (ListView)sender;
            _mList.ItemsSource = toVentas;
        }

        private void txtTotal_Loaded(object sender, RoutedEventArgs e)
        {
            _total = (TextBlock)sender;
        }

        private void txtSubTotal_Loaded(object sender, RoutedEventArgs e)
        {
            _subtotal = (TextBlock)sender;
        }

        private void txtIva_Loaded(object sender, RoutedEventArgs e)
        {
            _iva = (TextBlock)sender;
        }

    }
}
